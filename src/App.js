import React from 'react'
import './App.css'
import Stopwatch from './components/reducer/StopWatch'
import Registration from './components/Registration'
import UseMemo from './components/UseMemo'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import PassData from './components/PassData'
import Button from './components/Button'
function App () {
  return (
    <>
    <Router>
      <Button/>
      <Routes>
        <Route path='/' element={<Registration/>}/>
        <Route path='/passData' element={<PassData/>}/>
        <Route path='/useMemo' element={<UseMemo/>}/>
        <Route path='/stopWatch' element={<Stopwatch/>}/>
      </Routes>
    </Router>

    </>
  )
}

export default App
