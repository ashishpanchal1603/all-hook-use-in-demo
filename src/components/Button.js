import React from 'react'
import { Link, NavLink } from 'react-router-dom'
function Button () {
  return (
    <div className='btn'>
    <button className='space'><NavLink to="/">Registration Page</NavLink> </button>
    <button className='space'><NavLink to="/passData">Two hook in page</NavLink> </button>
    <button className='space'><Link to="/useMemo">UseMemo Page</Link> </button>
    <button><Link to="/stopWatch">StopWatch Page</Link> </button>
    </div>
  )
}

export default Button
