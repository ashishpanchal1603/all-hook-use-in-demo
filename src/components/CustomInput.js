import React, { forwardRef, useImperativeHandle } from 'react'
import PropTypes from 'prop-types'

function CustomInput (props, ref) {
  const { value } = props
  useImperativeHandle(ref, () => {
    return { alertHi: () => alert(`your output : ${value}`) }
  })
  return (
    <>
    <input type="text" style={{ backgroundColor: 'green', color: 'white', marginLeft: '450px', fontSize: '1.5rem' }} {...props} />
    </>
  )
}

export default forwardRef(CustomInput)

CustomInput.propTypes = {
  value: PropTypes.object
}
