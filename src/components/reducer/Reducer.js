const initialState = {
  isRunning: false,
  time: 0,
  type: ''
}

function reducer (state, action) {
  console.log('reducer state :>> ', state)
  switch (action.type) {
    case 'start':
      return { ...state, isRunning: true, type: 'start' }
    case 'stop':
      return { ...state, isRunning: false, type: 'stop' }
    case 'reset':
      return { isRunning: false, time: 0, type: 'reset' }
    case 'increment':
      return { ...state, time: state.time + 1 }
    default:
      alert('please enter correct button')
  }
}

export { reducer, initialState }
