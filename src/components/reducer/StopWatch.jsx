import React, { useReducer, useEffect } from 'react'
import { reducer, initialState } from './Reducer'

function Stopwatch () {
  const [state, dispatch] = useReducer(reducer, initialState)

  useEffect(() => {
    if (!state.isRunning) {
      return
    }
    const interval = setInterval(() => dispatch({ type: 'increment' }), 1000)
    return () => {
      clearInterval(interval)
    }
  }, [state.isRunning])

  return (
    <>
    <div className='container'>
      <h1> useReducer Hook</h1>
      <h1>{state.type}</h1>
     <h1> {state.time}s</h1>
    </div>
     <div className='btn'>
      <button className='space' onClick={() => dispatch({ type: 'start' })}>
        Start
      </button>
      <button className='space' onClick={() => dispatch({ type: 'stop' })}>
        Stop
      </button>
      <button className='space' onClick={() => dispatch({ type: 'reset' })}>
        Reset
      </button>
     </div>
    </>
  )
}
export default Stopwatch
