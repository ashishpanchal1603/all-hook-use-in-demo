import { useState } from 'react'

function CustomHook (initialValue) {
  const [value, setValue] = useState(initialValue)

  const reset = () => {
    setValue(initialValue)
  }

  const allValue = {
    value,
    onChange: e => {
      setValue(e.target.value)
    }
  }

  return [value, allValue, reset]
}
export default CustomHook
