import React, { useState, useMemo } from 'react'

const UseMemo = () => {
  const [count, setCount] = useState(0)
  const [item, setItem] = useState(10)

  const multipleCountMemo = useMemo(() =>
    count * 5
  , [count])

  const incrementCount = () => {
    setCount(count + 1)
  }

  const incrementItem = () => {
    setItem(item * 5)
  }
  return (
    <>
    <div className='App'>
        <h1>UseMemo Hook</h1>
        <h1>Count :{count}</h1>
        <h1>Item: {item}</h1>
        <h1>{multipleCountMemo}</h1>
        <div className='btn'>
        <button className='space' onClick={() => incrementCount()}>Update Count</button>
        <button className='space' onClick = {() => incrementItem()}>Update Item</button>
        </div>
    </div>
    </>
  )
}

export default UseMemo
