import React, { createContext } from 'react'
import Employee from './Employee'
const BioData = createContext()
function Student () {
  return (
    <>
    <BioData.Provider value={'jai Shree krishna'}>
     <Employee/>
    </BioData.Provider>
    </>
  )
}
export { BioData }
export default Student
