import React, { useEffect, useRef } from 'react'
import CustomHook from './CustomHook'

const Registration = () => {
  const name = useRef(null)

  const [firstName, setFirstName, resetFirstName] = CustomHook('')
  const [email, setEmail, resetEmail] = CustomHook('')
  const [Age, setAge, resetAge] = CustomHook('')

  // input ref
  useEffect(() => {
    name.current.focus()
  }, [])

  const handleSubmit = (e) => {
    e.preventDefault()
    alert(`firstName${firstName} and lastName ${email} and ${Age}`)
    resetFirstName()
    resetEmail()
    resetAge()
  }

  return (
    <>
      <div className="container">
        <div className="title">
          <h1>Registration Form with Create custom Hook</h1>
        </div>
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="name">
            <label
              htmlFor="userName"
            >
              userName
            </label>
            <br />
            <input type="text" name="name"
              id="name"
              ref={name}
             {...setFirstName}
              required={true}/>
          </div>
          <div className="Email">
            <label htmlFor="Email">Email</label>
            <br />
            <input
              type="email"
              name="email"
             {...setEmail}
              required={true}
            />
          </div>
          <div className="Age">
            <label htmlFor="Age">Age</label>
            <br />
            <input
              type="number"
              name="number"
             {...setAge}
              required={true}
            />
          </div>
          <div className="btn">
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    </>
  )
}

export default Registration
