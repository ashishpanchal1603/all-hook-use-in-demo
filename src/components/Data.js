import React, { useContext } from 'react'
import { BioData } from './Student'
import PropTypes from 'prop-types'

function Data (props) {
  const user = useContext(BioData)
  console.log('user', user)
  return (
    <>
    <h1>Data {user}</h1>
    <div>
        {props.data}
    </div>
    </>
  )
}

export default Data

Data.propTypes = {
  data: PropTypes.array
}
