import React, { useRef, useState } from 'react'
import CustomInput from './CustomInput'
import Student from './Student'
const PassData = () => {
  const [value, setValue] = useState('')
  const input = useRef('null')
  return (
    <>
      <div>
       <h1>Using useImperativeHandle and Also Use Context Hook</h1>
       <Student/>
       <CustomInput ref={input} value={value} onChange={e => setValue(e.target.value)}/>
      </div>
      <div className='btn'>
       <button onClick={() => input.current.alertHi()}>Alert</button>
      </div>
    </>
  )
}
export default PassData
